#!/usr/bin/python
""" cherrypy_example.py

    COMPSYS302 - Software Design
    Author: Andrew Chen (andrew.chen@auckland.ac.nz)
    Last Edited: 19/02/2018

    This program uses the CherryPy web server (from www.cherrypy.org).
"""
# Requires:  CherryPy 3.2.2  (www.cherrypy.org)
#            Python  (We use 2.7)

# The address we listen for connections on
listen_ip = "0.0.0.0"
listen_port = 10005

#global variables
logged_in = False
my_username = None
my_password = None
my_ip = None
my_location = None
message_destination = 'dlee906'
message_Sender = 'dlee906'
messageReceipt = '0'
currentPage = 'index'
my_key =''
pub_key =''

#import module
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from Crypto import Random
import cherrypy, urllib2, socket
import hashlib
import sqlite3
import re
import datetime
import json
import calendar
import time
import pyotp
import base64
from os.path import abspath
import mimetypes
import ctypes
import bcrypt
import binascii
import scrypt
import qrcode
from itertools import izip, cycle
from os.path import join as pjoin
import threading


class MainApp(object):

    #CherryPy Configuration
    _cp_config = {'tools.encode.on': True, 
                  'tools.encode.encoding': 'utf-8',
                  'tools.sessions.on' : 'True',
                 }
    

    
    # If they try somewhere we don't know, catch it here and send them to the right place.
    @cherrypy.expose
    def default(self, *args, **kwargs):
        """The default page, given when we don't recognise where the request is for."""
        Page = "I don't know where you're trying to go, so have a 404 Error."
        cherrypy.response.status = 404
        return Page

    # PAGES (which return HTML that can be viewed in browser)
    @cherrypy.expose
    def index(self):
        global currentPage
        currentPage = 'index'
        Page = '<style>button {background-color: aqua; color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;}'
        Page += 'p2{font-size: 150%; text-align: left}body{text-align: center; background-color: powderblue; font-size: 200%; font-family: verdana; margin: 100px; margin-top: 50px}'
        Page += 'div.row{display: flex}div.column{flex:50%; font-size: 70%; text-align: left; border: 3px solid blue; padding: 50px}'
        Page += 'h2{margin: 10px; text-align: center} p{margin: 10px; font-size: 150%; text-align: left}</style>'
                
        try: #Index Page when user logged in
            cherrypy.session['twoFA_password']
            alert = self.alert(messageReceipt)
            Page += alert
            Page += "<h2>Welcome to Jae'sbook</h2>"
            Page += '<div class="row"><div class="column"><p><b>Hello! ' + cherrypy.session['username'] + '!</b></p>'
            Page += '<form action="/signout" method="post" enctype="multipart/form-data"><input type="submit" value="Logoff"/></form>'
            Page += '<form action="/ping?sender=' + str(cherrypy.session['username']) + '" method="post" enctype="multipart/form-data"><input type="submit" value="Ping"/></form>'
            Page += self.readProfile(cherrypy.session['username']) + "</div>"
            Page += '<div class="column"><p2><b><form action="/onlineIndex" method="post" enctype="multipart/form-data">'
            Page += '<button input type="submit" value="Save My Profile"/>Online Menu</button></form>'
            Page += '<p><b>Change My profile </b></p><br/>'
            Page += '<form action="/saveMyProfile" method="post" enctype="multipart/form-data">'
            Page += '<b>Fullname:</b> <input type="text" name="fullname"/><br/>'
            Page += '<b>Position:</b> <input type="text" name="position"/><br/>'
            Page += '<b>Description:</b> <input type="text" name="description"/><br/>'
            Page += '<b>Picture:</b> <input type="text" name="url"/><br/><br/>'
            Page += '<input type="submit" value="Save My Profile"/></form></div>'
            Page += ''
                     
        except KeyError: #There is no username
            Page = '<style>body{text-align: center; background-color: powderblue; font-size: 200%; font-family: verdana; margin: 100px}'
            Page += 'h1{text-align: center; font-size: 150%; border: 5px solid blue; padding: 50px}</style>'
            Page += "<h1>Welcome! <br/> Jae'sbook!<br/><form action='/login' method='post' enctype='multipart/form-data'><input type='submit' value='Log In'/></form>"
            Page += '<image src="https://media.giphy.com/media/ypqHf6pQ5kQEg/giphy.gif"></h1>'
        return Page
    
    @cherrypy.expose 
    def onlineIndex(self): #if user click online menu
        global currentPage
        currentPage = 'onlineIndex'
        Page = '<style>button {background-color: aqua; color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;}'
        Page += 'input.menu{width 30em;height 3em;}input[type=submit]{width: 10em;height: 2em}p2{font-size: 150%;text-align: left}'
        Page += 'body{text-align: center;background-color: powderblue; font-size: 200%; font-family: verdana; margin: 100px; margin-top: 50px}'
        Page += 'div.row{display: flex}div.column{flex:50%;font-size: 70%;text-align: left; border: 3px solid blue; padding: 50px}h2{margin: 10px;text-align: center}'
        Page += 'p{margin: 10px;font-size: 150%;text-align: left}</style>'

        alert = self.alert(messageReceipt)
        Page += alert
 
        Page += '<body>'
        Page += "<h2>Welcome to Jae'sbook"
        Page +=  self.goMainButton() + '</h2>'
        Page += '<div class="row">'
        Page += '<div class="column"><p><b>Hello! ' + my_username + '!</b></p><form action="/signout" method="post" enctype="multipart/form-data"><input type="submit" value="Logoff"></form><form action="/ping?sender=' + my_username + '" method="post" enctype="multipart/form-data"><input type="submit" value="Ping"></form>' + self.readProfile(my_username)+ '</div>'

        Page += '<div class="column"><form action="/showChat" method="post" enctype="multipart/form-data"><button input type="submit" value="Chat" style="width: 30em;height 10em;"/>Chat</button></form>'
                    
        Page += '<p><b>Send a File</b></p>'
        Page += '<form action="/sendFile" method="post" enctype="multipart/form-data">'
        Page += '<b>Receiver:</b> <input type="text" name="destination"/><br/>'
        Page += '<b>File:</b> <input type="file" name="file_attach"/><br/>'
        Page += '<input type="submit" value="Send File"/></form><br/><br/>'

        Page += '<p><b>Users Profiles </b></p>'
        Page += '<form action="/readAllProfile" method="post" enctype="multipart/form-data">'
        Page += '<input type="submit" value="Get All Profile"/></form>'
        Page += '<form action="/receiveProfile" method="post" enctype="multipart/form-data">'
        Page += '<b>Profile username:</b> <input type="text" name="profile_username"/>'
        Page += '<input type="submit" value="Get Profile"/></form><br/><br/>'
                    
        Page += '<p><b>Group Members </b></p>'
        Page += '<form action="/setGroup" method="post" enctype="multipart/form-data">'
        Page += '<input type="submit" value="Set Group"/></form>'
        Page += '</div>'
        Page += '</div>'

        Page += '</body>'

        return Page
  
    @cherrypy.expose
    def login(self): #login page
        Page = '<style>body{ text-align: center; background-color: powderblue; font-size: 200%; font-family: verdana; border: 3px solid blue; padding: 50px; margin: 100px}</style>'
        #login form
        Page += '<body><form action="/signin" method="post" enctype="multipart/form-data">'
        Page += 'Username: <br/><input type="text" name="username"/><br/>'
        Page += 'Password: <br/><input type="password" name="password"/><br/>'
        Page += '<input type="submit" value="Login"/></form>'
        #video that welcome user
        Page += '<iframe width="420" height="315" src="https://www.youtube.com/embed/kE9rup_82gI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></body>'
        return Page

    

    #making a message box
    @cherrypy.expose
    def Mbox(title, text, style):
        return ctypes.windll.user32.MessageBoxW(0, text, title, style)

    #log off
    def logoff(self):
        print "logoff"
        username = cherrypy.session.get('username')
        password = cherrypy.session.get('password')
        if(urllib2.urlopen("http://cs302.pythonanywhere.com/logoff" + '?username=' + username + '&password=' + password + "&enc=" + '0').read()[0] == "0"):
            return 0
        else:
            return 1
        return Page
        
    # LOGGING IN AND OUT
    @cherrypy.expose
    def signin(self, username=None, password=None):
        """Check their name and password and send them either to the main page, or back to the main login screen."""
        error = self.authoriseUserLogin(username,password)
        if (error == 0):
            global logged_in
            logged_in=True
            threading.Timer(60, self.reLogin).start()
            conn = sqlite3.connect('key.db')
            c = conn.cursor()
            try:
                c.execute('CREATE TABLE twoFA (Username text PRIMARY KEY NOT NULL, Key text)')
            except:
                pass
            c.execute("SELECT * From twoFA")
            data = c.fetchall()
            for i in data:
                if(i[0] == username):
                    print "logged in before"
                    raise cherrypy.HTTPRedirect('/twoFA')
                else:
                    pass
                conn.commit()
            random_key = pyotp.random_base32()
            print random_key
            task = (
                username,
                random_key
                )
            #insert or replace profile
            c.execute('INSERT OR REPLACE INTO twoFA (Username, Key) Values(?,?)', task)
            conn.commit()
            conn.close()
            raise cherrypy.HTTPRedirect('/showtwoFA')
        else:
            raise cherrypy.HTTPRedirect('/login')

    @cherrypy.expose
    def signout(self):
        print "singout"
        """Logs the current user out, expires their session"""
        error = self.logoff()
        if(error == 0):
            global logged_in
            logged_in=False
            cherrypy.lib.sessions.expire()
        else:
            pass
        raise cherrypy.HTTPRedirect('/')

    #auto re logging in using threading
    def reLogin(self):
        print "beginning of relogin"
        if logged_in:
            threading.Timer(60, self.reLogin).start() #call function itself
            urllib2.urlopen("http://cs302.pythonanywhere.com/report?" + "username=" + my_username +
                            "&password=" + my_password + "&location=" + my_location +
                            "&ip=" + my_ip + "&port=" + str(listen_port))
            print "reLogin"
        else:
            threading.Thread.damon = True

    #ask 2FA
    @cherrypy.expose
    def twoFA(self, enter = None):
        
        Page = '<body><form action="/checktwoFA" method="post" enctype="multipart/form-data">'
        Page += '2FA: <br/><input type="text" name="enter"/><br/>'
        Page += '<input type="submit" value="Check it"/></form>'
        
        return Page

    #when logged in first time
    @cherrypy.expose
    def showtwoFA(self):
        conn = sqlite3.connect('key.db')
        c = conn.cursor()
        c.execute("SELECT * From twoFA")
        data = c.fetchall()
        for i in data:
            if(i[0] == my_username):
                key = i[1]
        totp = pyotp.TOTP(key)
        url = pyotp.totp.TOTP(key).provisioning_uri(my_username, issuer_name="2FA For Login Server")
        print url
        Page = '<body><form action="/checktwoFA" method="post" enctype="multipart/form-data">'
        Page += '2FA: <br/><input type="text" name="enter"/><br/>'
        Page += '<input type="submit" value="Check it"/></form>'

        img = qrcode.make(url)
        img.save(my_username + "_2FA.jpg")
        img.show()

        return Page

    #check 2FA
    @cherrypy.expose    
    def checktwoFA(self, enter = None):
        conn = sqlite3.connect('key.db')
        c = conn.cursor()
        c.execute("SELECT * From twoFA")
        data = c.fetchall()
        for x in data:
            if(x[0] == my_username):
                key = x[1]
                print key
        totp = pyotp.TOTP(key)
        print totp.now()
        print enter
        print("Current OTP:", totp.now())
        if(totp.verify(enter)):
            cherrypy.session['twoFA_password'] = True
            raise cherrypy.HTTPRedirect('/')
        else:
            raise cherrypy.HTTPRedirect('/twoFA')
        
        

    #check password is correct or not using API, authorisedUserLogin    
    def authoriseUserLogin(self, username, password):
        hashed_password = hashlib.sha256(password+username).hexdigest()
        #get internal ip
        ip = socket.gethostbyname(socket.gethostname())
        print ip
        port = str(listen_port)
        print port
        if(ip.find("10.103.") == 0):
            location = "0"
        elif(ip.find("172.24.") == 0):
            location = "1"
        else:
            location = "2"
            #get external ip
            request = (urllib2.urlopen("http://checkip.dyndns.org/").read())
            ip = re.findall(r"d{1,3}.d{1,3}.d{1,3}.d{1,3}", request)
            ip = re.findall(r"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", request)[0]
            print request
            print ip
            print "ip2"
        print location
        if (urllib2.urlopen("http://cs302.pythonanywhere.com/report"+'?username=' + username + '&password=' + hashed_password
                            + '&location=' + location + '&ip=' + ip + '&port=' + port + '&enc=' + '0').read()[0] == '0'):
            print socket.gethostbyname(socket.gethostname())
            cherrypy.session['ip'] = socket.gethostbyname(socket.gethostname())
            #set global variables for threading for auto relogin
            global my_username
            global my_password
            global my_location
            global my_ip
            global my_port
            my_username = username
            my_password = hashed_password
            my_location = location
            my_ip = socket.gethostbyname(socket.gethostname())
            my_port = port
            print cherrypy.session['ip']
            cherrypy.session['ip'] = ip
            #set cherrypy sessions
            cherrypy.session['port'] = port
            cherrypy.session['location'] = location
            cherrypy.session['username'] = username
            cherrypy.session['password'] = hashed_password
            print "variables in cherrypy"
            print port
            print location
            print username
            print hashed_password
            #create primary key
            self.createPEM()
            print "created primary key"
            return 0
        else:
            return 1

    #change stamp to date time
    def dateTime(self, stamp):
        return datetime.datetime.fromtimestamp(int(stamp)).strftime('%Y-%m-%d %H:%M:%S')

    #check online Users
    @cherrypy.expose
    def onlineUser(self):
        #load username and password
        username = cherrypy.session.get('username')
        password = cherrypy.session.get('password')
        error = self.getList(username, password)
        if(error == 0):
            Page = ''
            #get list using API
            List = urllib2.urlopen("http://cs302.pythonanywhere.com/getList"+'?username='+ username + '&password=' + password + '&enc=' +'0' +'&json=' + '1').read()
            data = json.loads(List)
            print data
            #open database called List
            conn = sqlite3.connect('List.db')
            c = conn.cursor()
            #create Table on database if there is no table yet
            try:
                c.execute('CREATE TABLE Lists (username text PRIMARY KEY NOT NULL, location text, ip text , port text, lastLogin text)')
            except:
                pass

            insert = 'INSERT OR REPLACE INTO Lists (username, location, ip, port, lastLogin) Values(?,?,?,?,?)'

            for i in data:
                for key, value in data[i].items():
                    Page += key + ': ' + value + '<br/>'
                task = (
                    data[i].get('username',''),
                    data[i].get('location',''),
                    data[i].get('ip',''),
                    data[i].get('port',''),
                    data[i].get('lastLogin','')
                    )
                Page += '<br/>'
                #insert variables on table
                c.execute(insert, task)
                conn.commit()
            conn.close()
            
            return Page
        else:
            raise cherrypy.HTTPRedirect('/')
        
    #set groups
    @cherrypy.expose
    def setGroup(self):
        Page = '<form action="/groupMessage" method="post" enctype="multipart/form-data">'
        Page += 'Enter group members to set Group! <br/>'
        Page += 'Group Members: <input type="text" name="groupMembers"/>'
        Page += '<input type="submit" value="setGroup"/></form>'
        return Page
        
    #get group id
    def getGroupID(self, username, password, groupMembers):
        
        groupID = (urllib2.urlopen("http://cs302.pythonanywhere.com/setGroup"+
                           '?username='+ str(username) + '&password=' + str(password) +
                           '&groupMembers=' + str(groupMembers) + '&enc=' +'0').read())
        if(groupID[0]=='0'):
            cherrypy.session['groupID'] = groupID[3:]
            return 0
        else:
            return 1

    #group ID
    @cherrypy.expose
    def groupID(self, groupMembers = None):
        username = cherrypy.session.get('username')
        password = cherrypy.session.get('password')
        #check error
        error = self.getGroupID(username, password, groupMembers)
        if (error == 0):
            groupID = cherrypy.session.get('groupID')
            Page = ''
            print username
            print groupID
            groupMembers = (urllib2.urlopen("http://cs302.pythonanywhere.com/getGroup"+
                           '?username='+ str(username) + '&password=' + str(password) +
                           '&groupID=' + str(groupID) + '&enc=' +'0').read())
            #print group ID
            Page += 'groupID : ' + groupMembers[3:19] + '<br/>'
            #print group members
            Page += 'group Members : ' + groupMembers[20:]
            return Page
        else:
            Page = 'There is no Group yet'
            return Page

    #get group members
    @cherrypy.expose
    def groupMessage (self, groupMembers = None):
        alert = self.alert(messageReceipt)
        Page = alert
        Page += self.groupID(groupMembers)
        Page += '<form action="/sendGroupMessage" method="post" enctype="multipart/form-data">'
        Page += 'Group ID: <input type="text" name="groupID"/><br/>'
        Page += 'Message: <input type="text" name="message"/>'
        Page += '<input type="submit" value="sendMessage"/></form>'
        return Page

    #send group message
    @cherrypy.expose
    def sendGroupMessage(self, groupID = None, message = None):
        sender = my_username
        password = my_password
        print sender
        print groupID
        groupMembers = (urllib2.urlopen("http://cs302.pythonanywhere.com/getGroup"+
                           '?username='+ str(sender) + '&password=' + str(password) +
                           '&groupID=' + str(groupID) + '&enc=' +'0').read())
        #Group members
        print groupMembers
        memberList = groupMembers[20:]
        print "MEMBER LIST"
        print memberList
        List = urllib2.urlopen("http://cs302.pythonanywhere.com/getList"+'?username='+ sender + '&password=' + password + '&enc=' +'0' +'&json=' + '1').read()
        data = json.loads(List)
        for i in data:
            if(memberList.find(data[i].get('username')) >=0):
                destination = data[i].get('username')
                ip = data[i].get('ip')
                port = data[i].get('port')
                print("ip is " + ip + "     port is " + port)
                #make parameters
                parameters = {'sender': sender, 'destination': data[i].get('username'), 'message': message, 'stamp': (time.time())}
                print parameters
                #make json object
                json_object = json.dumps(parameters)
                req = urllib2.Request("http://" + (ip) + ":" + (port) + "/receiveMessage", json_object, {"Content-Type" :'application/json'})
                response = urllib2.urlopen(req)
                error = response.read()
                print error
                if(error[0] == "0"): #if there is no error
                    self.saveMessage(parameters)
                    global messageReceipt
                    messageReceipt= '1'
                    pass
                else: #if there is error
                    error_message = self.errorMessage(error)
                    Page = error_message
                    back_button = self.backButton()
                    Page += back_button
                    return Page
        raise cherrypy.HTTPRedirect('/groupMessage')
            
    #get error code for getGroup
    def getGroup(self, username, password, groupID):
        if(urllib2.urlopen("http://cs302.pythonanywhere.com/getGroup"+
                           '?username='+ str(username) + '&password=' + str(password) +
                           '&groupID=' + str(groupID) + '&enc=' +'0').read()[0]=='0'):
            return 0
        else:
            return 1

    #get error code for getList
    def getList(self, username, password):
        if(urllib2.urlopen("http://cs302.pythonanywhere.com/getList"+'?username='+ str(username) + '&password=' + str(password) + '&enc=' +'0' +'&json=' + '1').read()[2] == '0'):
            
            return 0
        else:
            return 1

    #give error code when listUser api is called
    def listUser(self, username):
        userList = (urllib2.urlopen("http://cs302.pythonanywhere.com/listUsers").read())
        if(userList.find(username) >= 0):
            return 0
        else:
            return 1

    #check my ping    
    @cherrypy.expose    
    def ping(self, sender):
        return "0"

    #error messages
    @cherrypy.expose
    def errorMessage(self, error):
        Page =''
        if(error == '0'):
            Page += "Action was successful"
        elif(error == '1'):
            Page += "Missing Compulsory Field"
        elif(error == '2'):
            Page += "Unauthenticated User"
        elif(error == '3'):
            Page += "Client Currently Unavailable"
        elif(error == '4'):
            Page += "Database Error"
        elif(error == '5'):
            Page += "Timeout Error"
        elif(error == '6'):
            Page += "Insufficient Permissions"
        elif(error == '7'):
            Page += "Hash does not match"
        elif(error == '8'):
            Page += "Encoding Not Supported"
        elif(error == '9'):
            Page += "Encryption Standard Not Supported"
        elif(error == '10'):
            Page += "Hashing Standard Not Supported"
        elif(error == '11'):
            Page += "Rate Limited"
        elif(error == '12'):
            Page +=  'File Size Too Large'
        elif(error == '15'):
            Page += "Clinet do not support this function"
        return Page

    #make a button that can go back to index page
    def goMainButton(self):
        Page = '<form action="/" method="post" enctype="multipart/form-data">'
        Page += '<input type="submit" value="Go back to main Menu" style="height:50px; width:30em"/></form>'
        return Page

    #make a button that can go back to online menu
    def backButton(self):
        Page = '<form action="/onlineIndex" method="post" enctype="multipart/form-data">'
        Page += '<input type="submit" value="Go back to Online Menu" style="height:50px; width:30em"/></form>'
        return Page

    #alert
    def alert(self, receipt):
        print "This is RECEIPT"
        print receipt
        global messageReceipt
        messageReceipt = '0'
        print receipt
        Page = '<style>div.alert {padding: 20px;color: white;opacity: 1;transition: opacity 0.6s;margin-bottom: 15px;background-color: #2196F3;}'
        Page += 'span.closebtn {margin-left: 15px;color: white;font-weight: bold;float: right;font-size: 22px;line-height: 20px;cursor: pointer;transition: 0.3s;}'
        Page += 'span.closebtn:hover {color: black;}</style>'
        #send alert
        if(str(receipt) == '1'):
            Page += '<div class="alert send">'
            Page += '<span class="closebtn" onclick="this.parentElement.style.display=' + "'none'" + ';">&times;</span>'
            Page += '<strong>Sucess!</strong> Message went well'
            Page += '</div>'
        #receive alert
        elif(str(receipt) == '2'):
            Page += '<div class="alert read">'
            Page += '<span class="closebtn" onclick="this.parentElement.style.display=' + "'none'" + ';">&times;</span>'
            Page += '<strong>Hi!</strong> You got a new message<br/>'
            Page += '<a href="/showChat?chat_username=' + message_Sender + '">from : ' + message_Sender + '</a>'
            Page += '</div>'
        elif(str(receipt) == '3'):
            Page += '<div class="alert delete">'
            Page += '<span class="closebtn" onclick="this.parentElement.style.display=' + "'none'" + ';">&times;</span>'
            Page += '<strong>Sucess!</strong> Message is deleted'
            Page += '</div>'
        else:
            Page = ''
        return Page
            
    #send message
    @cherrypy.expose  
    def sendMessage(self, message = None):
        print "Start send Message"
        print cherrypy.session.get('username')
        print message_destination
        sender = cherrypy.session.get('username')
        password = cherrypy.session.get('password')
        ip = None
        port = None
        List = urllib2.urlopen("http://cs302.pythonanywhere.com/getList"+'?username='+ sender + '&password=' + password + '&enc=' +'0' +'&json=' + '1').read()
        data = json.loads(List)
        for i in data:
            if(str(message_destination) == data[i].get('username')):
                ip = data[i].get('ip')
                port = data[i].get('port')
                print("ip is " + ip + "     port is " + port)
                print data[i].get('location')
                #make parameters
                parameters = {'sender': sender, 'destination': message_destination, 'message': message, 'stamp': (time.time())}
                print parameters
                #make json object
                json_object = json.dumps(parameters)
                req = urllib2.Request("http://" + (ip) + ":" + (port) + "/receiveMessage", json_object, {"Content-Type" :'application/json'})
                response = urllib2.urlopen(req)
                error = response.read()
                print error
                if(error[0] == "0"): #if there is no error
                    self.saveMessage(parameters)
                    global messageReceipt
                    messageReceipt= '1'
                    raise cherrypy.HTTPRedirect('/showChat?chat_username=' + message_destination)
                else: #if there is error
                    error_message = self.errorMessage(error)
                    Page = error_message
                    back_button = self.backButton()
                    Page += back_button
                    return Page

        #if ip or port cannot be found        
        if(ip == None or port == None):
            error_message = self.errorMessage('2')
            Page = error_message
            back_button = self.backButton()
            Page += back_button
            return Page

        #receive message
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def receiveMessage(self):
        #get json object
        print "message receive function run"
        input_data = cherrypy.request.json
        sender = input_data['sender']
        global message_Sender
        message_Sender = sender
        print message_Sender
        if(self.ping(input_data["sender"]) == "0"):
            try:
                data= {
                    "sender" : (input_data["sender"]),
                    "destination" : (input_data["destination"]),
                    "message" : (input_data["message"]),
                    "stamp" : (input_data["stamp"])
                    }
                #save Meesage
                self.saveMessage(data)
                
                #refresh pages
                global messageReceipt
                messageReceipt= '2'
                print "THIS IS MESSAGE RECEIPT 2"
                print messageReceipt
                try:
                    self.receiveAlert()
                except:
                    pass
                return "0"
            except:
                return "1"            
        else:
            return "3"

    @cherrypy.expose
    def receiveAlert(self):
        if(currentPage == 'showChat'):
            print "showChat alert"
            print message_destination
        
            print 
            raise cherrypy.HTTPRedirect('/showChat')
            print "whyyyy"
        elif(currentPage == 'index'):
            print "index alert"
            raise cherrypy.HTTPRedirect('/')
        elif(currentPage == 'OnlineIndex'):
            print "online Index alert"
            raise cherrypy.HTTPRedirect('/onlineIndex')
        

    #read previous and current messages
    @cherrypy.expose 
    def readMessage(self, myname, username):
        #declare self Message value
        selfMessage = False
        Page = ''
        #open database
        conn = sqlite3.connect('Message.db')
        c = conn.cursor()
        #select all data from messages
        c.execute("SELECT * From Messages")
        data = c.fetchall()
        print "read Message"
        print "myname"
        print myname
        print username
        prevTime = ''
        prevSender = ''
        lastTime = ''
        for i in data:
            #check a message whether self message or not
            #if message is a self message, make them not repeat twice
            if(selfMessage == True):
                selfMessage = False
                print "pass"
                pass
            else:
                #give them back messages only talked with receiver
                if(myname == i[0] and username == i[1]):
                    word = 0
                    length_message = len(i[2])
                    dateTime = self.dateTime(i[3])
                    time = dateTime[0:16]
                    if(prevTime == ''):
                        prevTime = time
                    if(prevSender == myname): 
                        if(time != prevTime):
                            Page += "<p3><b>Time : </b>" + str(prevTime) + '</p3><br/>'
                            prevTime = time
                        else:
                            pass
                    elif(prevSender == username):
                        Page += "<p4><b>Time : </b>" + str(prevTime) + '</p4><br/>'
                        prevTime = time
                    while(length_message > 25):
                        Page += '<p1>' + i[2][word:word+24] + '</p1><br/>'
                        length_message -= 25
                        word += 24
                    Page += '<p1>' + i[2][word:] + '</p1><br/>'
                    Page += '<form action="/deleteMessage" method="get"><button name="stamp" class="delete" type="submit" value=' + str(i[3]) + '>DELETE</button><br/></form>'
                    print "made button delete"
                    prevSender = myname
                    lastTime = '0'
                    print lastTime
                elif(myname == i[1] and username == i[0]):
                    word = 0
                    length_message = len(i[2])
                    dateTime = self.dateTime(i[3])
                    time = dateTime[0:16]
                    if(prevTime == ''):
                        prevTime = time
                    if(prevSender == username): 
                        if(time != prevTime):
                            Page += "<p4><b>Time : </b>" + str(prevTime) + '</p4><br/>'
                            prevTime = time
                        else:
                            pass
                    elif(prevSender == myname):
                        Page += "<p3><b>Time : </b>" + str(prevTime) + '</p3><br/>'
                        prevTime = time
                    while(length_message > 25):
                        Page += '<p2>' + i[2][word:word+24] + '</p2><br/>'
                        length_message -= 25
                        word += 24
                    Page += '<p2>' + i[2][word:] + '</p2><br/>'
                    Page += '<p2><form action="/deleteMessage" method="get"><button name="stamp" class="delete" type="submit" value=' + str(i[3]) + '>DELETE</button></form></p2><br/><br/>'
                    prevSender = username
                    lastTime = '1'
                if(myname == i[1] and i[0] == myname):
                    selfMessage = True
                else:
                    selfMessage= False
            print lastTime
            conn.commit()
        print lastTime
        
        print "before Last Time"
        if(lastTime == '0'):
            Page += "<p3><b>Time : </b>" + str(prevTime) + '</p3><br/>'
        elif(lastTime == '1'):
            Page += "<p4><b>Time : </b>" + str(prevTime) + '</p4><br/>'
        print "after last Time"
        print "finish read message from"
        conn.close()
        return Page

    def showChatList(self):
        global my_username
        global my_password
        sender = my_username
        password = my_password
        print sender
        print password
        Page = ''
        username_list=[sender]
        username_onlinelist = []
        username_offlinelist = []
        #open database
        conn = sqlite3.connect('Message.db')
        c = conn.cursor()
        #select all data from unread_Messages
        try:
            c.execute('CREATE TABLE unread_Messages (sender text PRIMARY KEY NOT NULL, unread_message real)')
        except:
            pass
        c.execute("SELECT * From unread_Messages")
        data_unread = c.fetchall()
        List = urllib2.urlopen("http://cs302.pythonanywhere.com/getList"+'?username='+ sender + '&password=' + password + '&enc=' +'0' +'&json=' + '1').read()
        data = json.loads(List)
        #get online user
        Page += '<div class="column chatlist" style="width = 30%;">Online'
        for i in data:
            unread_message = 0
            for y in data_unread:
                if(data[i].get('username') == y[0]):
                    unread_message = y[1]
                conn.commit()
            username_onlinelist.append(str(data[i].get('username')) +" Unread_Message:" + str(unread_message))
            username_list.append(data[i].get('username'))
        Page += '<form action="/showChat" method="get">'
        for x in username_onlinelist:
            Page += '<button name="chat_username" type="submit" value=' + str(x) + '>' + str(x) + '</button>'
            Page += '<br/>'
        Page += '</form>'
       
        
        #select all data from messages
        c.execute("SELECT * From Messages")
        data = c.fetchall()
        Page += '<br/>Offline'
        for i in data:
            if i[0] in username_list:
                pass
            else:
                for y in data_unread:
                    if(i[0] == y[0]):
                        username_list.append(i[0])
                        username_offlinelist.append(i[0] + " Unread_Message:" +str(y[1]))
                    conn.commit()
            if i[1] in username_list:
                pass
            else:
                for y in data_unread:
                    if([1] == y[0]):
                        username_list.append(i[1])
                        username_offlinelist.append(i[1] + " Unread_Message:" +str(y[1]))
                    conn.commit()
            conn.commit()
        conn.close()
        Page += '<form action="/showChat" method="get">'
        #show offline Users who you talked before
        print username_offlinelist
        if(username_offlinelist == []):
            Page += "<br/>No offline Users"
        else:
            for x in username_offlinelist:
                Page += '<button name="chat_username" type="submit" value=' + str(x) + '>' + str(x) + '</button>'
                Page += '<br/>'
        Page += '</form>'
        return Page

    @cherrypy.expose
    def showChat(self, chat_username = None):
        global currentPage
        currentPage = 'showChat'
        print "start show Chat"
        print "myname"
        global my_username
        myname = my_username
        
        print myname
        global message_destination
        if(chat_username == None):
            message_destination = myname
        else:
            message_destination = chat_username
        print "THIS IS MESSAGE DESTINATION"
        print message_destination
        #open database
        print "show Chat opendatabase"
        conn = sqlite3.connect('Message.db')
        c = conn.cursor()
        #select all data from unread_Messages
        try: #create table if there is no table yet
            c.execute('CREATE TABLE Messages (sender text, destination text, message text, stamp real)')
        except:
            pass
        try:
            print "open table"
            c.execute("SELECT * From unread_Messages")
            data_unread = c.fetchall()
            print data_unread
            for y in data_unread:
                if(y[0] == message_destination):
                    unread_message = 0
            task = (
                message_destination,
                unread_message
            )
            print "task"
            print task
            #insert variables to table
            c.execute('INSERT OR REPLACE INTO unread_Messages (sender, unread_message) Values(?,?)', task)
            conn.commit()
            conn.close()
            print "finish insert or replace unread_message"
        except:
            pass
        
        print "start Page print"  
        Page = '<style>button {background-color: aqua; color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;}'
        Page += 'button.delete{background-color: aqua; color: white;padding: 2px 4px;font-size: 3px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;}'
        Page += 'input[type=submit]{width: 10em;height: 2em}body{text-align: center;background-color: powderblue; font-size: 200%; font-family: verdana; margin: 100px; margin-top: 50px}'
        Page += 'div.row{display: flex; height: 80%;}div.column{overflow-y: auto;flex:70%;font-size: 70%;text-align: left; border: 3px solid blue; padding: 50px}div.column.chatlist{flex:30%}'
        Page += 'h2{margin: 10px;text-align: left;}h3{margin: 10px;text-align: right}'
        Page += 'p1{margin: 10px;font-size: 50%;float:left;}p2{margin: 10px;font-size: 50%; float: right;text-align: right}'
        Page += 'p3{font-size: 20%;float:left;text-align: left;}p4{font-size: 20%;float:right;text-align: right}</style>'
        print messageReceipt
        alert = self.alert(messageReceipt)
        print "THIS IS ALERT"
        print alert
        Page += alert
        Page += "<b>Welcome to Jae'sbook Chat</b>"
        back_button = self.backButton()
        Page += back_button
        
        try:
            print message_destination
            Page += '<h2><p1><b> Sender: ' + str(message_destination) + '</b></p1><br/>'
            Page += '<form action="/sendMessage" method="post" enctype="multipart/form-data">'
            Page += '<input type="text" name="message"/><br/>'
            Page += '<input type="submit" value="Send Message"/></form>'
            Page += '<div class="row"><div class="column">' + self.readMessage(message_destination, myname) + '</div>'
            print "finish readMessages"
        except:
            pass
        Page += self.showChatList() + '</div>'
        return Page
    
    #save Message        
    def saveMessage(self, data):
        #open database
        stamp = (round(data.get('stamp'), 2))
        conn = sqlite3.connect('Message.db')
        c = conn.cursor()
        try: #create table if there is no table yet
            c.execute('CREATE TABLE Messages (sender text, destination text, message text, stamp real)')
        except:
            pass
        task = (
            data.get('sender'),
            data.get('destination'),
            data.get('message'),
            stamp
            )
        #insert variables to table
        c.execute('INSERT INTO Messages (sender, destination, message, stamp) Values(?,?,?,?)', task)
        conn.commit()
        conn.close()
        print "save Messages table"
        conn = sqlite3.connect('Message.db')
        c = conn.cursor()
        unread_message = 0
        print 'open database'
        try: #create table if there is no table yet
            c.execute('CREATE TABLE unread_Messages (sender text PRIMARY KEY NOT NULL, unread_message real)')
            unread_message = 0
        except:
            c.execute("SELECT * From unread_Messages")
            data_unread = c.fetchall()
            for i in data_unread:
                unread_message = i[1] + 1
        print "task error"
        print data.get('sender')
        print unread_message
        task = (
            data.get('sender'),
            unread_message
            )
        print task
        #insert variables to table
        c.execute('INSERT OR REPLACE INTO unread_Messages (sender, unread_message) Values(?,?)', task)
        conn.commit()
        print "where is a problem"
        conn.close()

    #delete message
    @cherrypy.expose
    def deleteMessage(self, stamp = None):
        print "delete Message"
        print stamp
        destination = message_destination
        hashing = 3
        encryption = 3
        sender = my_username
        password = my_password
        myMessage = "This is my Message"
        my_Message = self.encrypt(encryption, myMessage)
        print my_Message
        data = json.loads(my_Message)
        my_Message = data.get('message')
        conn = sqlite3.connect('Message.db')
        c = conn.cursor()
        c.execute("SELECT * From Messages")
        table = c.fetchall()

        List = urllib2.urlopen("http://cs302.pythonanywhere.com/getList"+'?username='+ sender + '&password=' + password + '&enc=' +'0' +'&json=' + '1').read()
        data = json.loads(List)
        for i in data:
            if(str(destination) == data[i].get('username')):
                ip = data[i].get('ip')
                port = data[i].get('port')
                print("ip is " + ip + "     port is " + port)
                parameters = {'sender': sender, 'destination': destination, 'stamp': stamp, 'message': my_Message, 'encryption': encryption}
                print parameters
                json_object = json.dumps(parameters)
                print "GO TO HANDSHAKE"
                #check handshake
                req = urllib2.Request("http://" + (ip) + ":" + (port) + "/handshake", json_object, {"Content-Type" :'application/json'})
                response = urllib2.urlopen(req)
                error = response.read()
                print "ERROR"
                print error
                handshake_data = json.loads(error)
                error = handshake_data.get('error')
                print error
                if(str(error) == '0'):
                    print "There is no error on handshake"
                    #get handshake data
                    handshake_message = handshake_data.get('message')
                    print handshake_message
                    print myMessage
                    if(handshake_message == myMessage):
                        print "handshake is successs"
                        pass
                    else:
                        return "8"
                
        conn = sqlite3.connect('Message.db')
        c = conn.cursor()
        c.execute("SELECT * From Messages")
        table = c.fetchall()
        message = ''
        for i in table:
            if((i[0]==sender and i[1] ==destination) or (i[1] == sender and i[0] == destination)):
                if(str(i[3])==stamp):
                    message = i[2]
                    print message
                    
        #hashing
        if(hashing == 1):
            hashed_message = hashlib.sha256(message).hexdigest()
        elif(hashing == 2):
            hashed_message = hashlib.sha256(message + sender).hexdigest()
        elif(hashing == 3):
            hashed_message = hashlib.sha512(message + sender).hexdigest()
        elif(hashing == 4):
            hashed_message = binascii.hexlify(bcrypt.hashpw(message, sender))
        elif(hashing == 5):
            hashed_message = binascii.hexlify(scrypt.hash(message, sender))
        elif(hashing == 0):
            pass
        else:
            return '10'

        print "hashed_message"
        print hashed_message
        #encrypt message
        encrypted_message = self.encrypt(encryption, hashed_message)
        data =json.loads(encrypted_message)
        error = data.get('error')
        message = data.get('message')
        print message
        #encrypt stamp
        encrypted_stamp = self.encrypt(encryption, stamp)
        data = json.loads(encrypted_stamp)
        encrypted_stamp = data.get('message')
        parameters = {'sender': sender, 'destination': destination, 'stamp': encrypted_stamp, 'hashing': hashing, 'hash': message, 'encryption': encryption}
        json_object = json.dumps(parameters)
        req = urllib2.Request("http://" + (ip) + ":" + (port) + "/acknowledgeDelete", json_object, {"Content-Type" :'application/json'})
        response = urllib2.urlopen(req)
        error = response.read()
        print "PLS TYPE IN STRING"
        if(str(error) == '0'):
            try:
                #delete message from database
                print "Delete My dataBase"
                conn = sqlite3.connect('Message.db')
                c = conn.cursor()
                print sender
                print destination
                print stamp

                c.execute('DELETE FROM Messages WHERE sender=? AND destination=? AND stamp=?', (sender, destination, float(stamp)))
                conn.commit()
                c.execute('DELETE FROM Messages WHERE sender=? AND destination=? AND stamp=?', (destination, sender, float(stamp)))
                conn.commit()
                conn.close()
                print "DELETED WELL"
            except:
                return '4'
        else:
            error_message = self.errorMessage(error)
            Page = error_message
            back_button = self.backButton()
            Page += back_button
            return Page

    #acknowledge
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def acknowledge(self):
        input_data = cherrypy.request.json
        print "acknowledge"
        print input_data['hash']
        print "hash"
        sender = input_data['sender']
        conn = sqlite3.connect('Message.db')
        c = conn.cursor()
        c.execute("SELECT * From Messages")
        table = c.fetchall()
        message = ''
        print "check table"
        for i in table:
            if((i[0]==input_data['sender'] and i[1] == input_data['destination']) or (i[1] == input_data['sender'] and i[0] == input_data['destination'])):
                print "get Data"
                print i[2]
                print i[3]
                print input_data['stamp']
                if(str(i[3])==input_data['stamp']):
                    message = i[2]
                    print message
        #if there is no meesage
        if(message == None):
            return "4"
        else:
            #check hashing and compare hashed message
            if(input_data['hashing'] == 1):
                hashed_message = hashlib.sha256(message).hexdigest()
            elif(input_data['hashing'] == 2):
                hashed_message = hashlib.sha256(message + sender).hexdigest()
            elif(input_data['hashing'] == 3):
                hashed_message = hashlib.sha512(message + sender).hexdigest()
            elif(input_data['hashing'] == 4):
                print "hashing 4"
                hashed_message = binascii.hexlify(bcrypt.hashpw(message, sender))
            elif(input_data['hashing'] == 5):
                print "hashing 4"
                hashed_message = binascii.hexlify(scrypt.hash(message, sender))
            elif(input_data['hashing'] == 0):
                pass
            else:
                return '10'
            print "COMPARE HASH"
            print hashed_message
            print input_data['hash']
            #if given hashed message is same as my hashed message
            if(input_data['hash'] == hashed_message):
                return '0'
            else:
                #if hashed message is wrong
                return '7'

    #deleting message with encryption and hashing
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def acknowledgeDelete(self):
        #open database
        input_data = cherrypy.request.json
        sender = input_data['sender']
        destination = input_data['destination']
        stamp = input_data['stamp']
        encryption = input_data['encryption']
        #decrypt stamp
        stamp = self.decrypt(encryption, stamp)
        data = json.loads(stamp)
        stamp = data.get('message')
        print stamp
        #hashing message
        hashing = input_data['hashing']
        hashed_message = input_data['hash']        
        if(sender == None or destination == None or stamp == None):
            return "1"
        elif(hashing == None):
            return '10'
        elif(encryption == None):
            return '9'
        else:
            pass
        if(self.ping(input_data['sender']) == "0"):
            pass
        else:
            return "3"
        print "hashed_message"
        print hashed_message
        print "DECRYPT MESSAGE PLS <BR/><BR/>"
        #decrypt message
        decrypted_message = self.decrypt(encryption, hashed_message)
        data = json.loads(decrypted_message)
        decrypted_message = data.get('message')
        parameters = {'sender': sender, 'destination': destination, 'stamp': stamp, 'hashing': hashing, 'hash': decrypted_message }
        print parameters
        json_object = json.dumps(parameters)
        req = urllib2.Request("http://" + my_ip + ":" + my_port + "/acknowledge", json_object, {"Content-Type" :'application/json'})
        print "req"
        response = urllib2.urlopen(req)
        error = response.read()
        if(error == '0'):
            try:
                print sender
                print destination
                print stamp
                print float(stamp)
                conn = sqlite3.connect('Message.db')
                c = conn.cursor()
                print "delete function"
                c.execute('DELETE FROM Messages WHERE sender=? AND destination=? AND stamp=?', (sender, destination, float(stamp)))
                conn.commit()
                c.execute('DELETE FROM Messages WHERE sender=? AND destination=? AND stamp=?', (destination, sender, float(stamp)))
                conn.commit()
                print "Deleted"
                conn.close() 
                
                return '0'
            except:
                return '4'
        else:
            return error

    #create public and private key
    def createPEM(self):  
        private_key = RSA.generate(1024, Random.new().read)
        publickey = private_key.publickey()
        global my_key
        global pub_key
        pub_key = binascii.hexlify(publickey.exportKey('DER'))
        my_key = binascii.hexlify(private_key.exportKey('DER'))
        
    #read and import key
    def readPEM(self, key): 
        key = binascii.unhexlify(key)
        key = RSA.importKey(key)
        return key
    
    #get pub_key
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def getPublicKey(self):
        input_data = cherrypy.request.json
        sender = input_data['sender']
        username = input_data['username']
        List = urllib2.urlopen("http://cs302.pythonanywhere.com/getList"+'?username='+ my_username + '&password=' + my_password + '&enc=' +'0' +'&json=' + '1').read()
        data = json.loads(List)
        for i in data:
            if(data[i].get('username') == sender):
                parameters = {'error': '0', 'pubkey': pub_key}
                response = json.dumps(parameters)
                return response 
            else:
                pass
        return '2'
    
    #encrypt
    def encrypt(self, encryption, message):
        if(encryption == 0):
            return message
        #XOR
        elif(encryption == 1):
            key = '10010110'
            xored = ''.join(chr(ord(x) ^ ord(y)) for (x,y) in izip(message, cycle(key)))
            parameters = {'error': '0', 'message': binascii.hexlify(xored)}
            response = json.dumps(parameters) 
            return response
        #AES
        elif(encryption == 2):
            key = '41fb5b5ae4d57c5ee528adb078ac3b2e'
            message = message + ((16 - len(message) % 16) * '')
            iv = ''*16
            cipher = AES.new(key, AES.MODE_CBC, iv)
            parameters = {'error': '0', 'message': binascii.hexlify(iv + cipher.encrypt(message))}
            response = json.dumps(parameters) 
            return response
        #RSA
        elif(encryption == 3):
            sender = my_username
            destination = message_destination
            password = my_password
            List = urllib2.urlopen("http://cs302.pythonanywhere.com/getList"+'?username='+ sender + '&password=' + password + '&enc=' +'0' +'&json=' + '1').read()
            data = json.loads(List)
            for i in data:

                if(destination == data[i].get('username')):
                    ip = data[i].get('ip')
                    port = data[i].get('port')
                    parameters = {'sender': sender, 'username': destination}
                    json_object = json.dumps(parameters)
                    print ("ip is " + ip)
                    print parameters
                    req = urllib2.Request("http://" + (ip) + ":" + (port) + "/getPublicKey", json_object, {"Content-Type" :'application/json'})
                    response = urllib2.urlopen(req)
                    error = response.read()
                    error = json.loads(error)
                    print error
                    print error.get('error')
                    if(str(error.get('error')) == '0'):
                        print "good"
                        publickey = error.get('pubkey')
                        publickey = self.readPEM(publickey)
                        publickey = publickey.publickey()
                        encrypted_msg = (''.join(publickey.encrypt(str(message), 128)))
                        parameters = {'error': '0', 'message': (binascii.hexlify(encrypted_msg))}
                        response = json.dumps(parameters)
                        print "no error"
                        return response
                    else:
                        return (error.get('error'))
                    
        elif(encryption == 4 or encryption == 5):
            print "not made for encrypt 4 and 5"
            return '15'
        else:
            return '9'

    #decrypt code
    def decrypt(self, encryption, message):
        if(encryption == 0):
            return message
        #XOR decrypt
        elif(encryption == 1):
            message = binascii.unhexlify(message)
            key = '10010110'
            parameters = {'error': '0', 'message': (''.join(chr(ord(x) ^ ord(y)) for (x,y) in izip(message, cycle(key))))}
            response = json.dumps(parameters)
            return response
        #decrypt AES
        elif(encryption == 2):
            key = '41fb5b5ae4d57c5ee528adb078ac3b2e'
            message = binascii.unhexlify(message)
            iv = message[:16]
            cipher = AES.new(key, AES.MODE_CBC, iv )
            parameters = {'error': '0', 'message': cipher.decrypt(message[16:]).rstrip('')}
            response = json.dumps(parameters)
            return response
        #RSA
        elif(encryption == 3):
            print "Start DECRYPTION"
            print message
            key = self.readPEM(my_key)
            enc = binascii.unhexlify(message)
            print enc
            new = key.decrypt(enc)
            print new
            print "NOOOOOOOOOOOOOOOOOOOOOOOOOOOOO"
            parameters = {'error': '0', 'message': new}
            print parameters
            response = json.dumps(parameters) 
            return response
        elif(encryption == 4 or encryption == 5):
            print "not made for decrypt 4 and 5"
            return '15'
        else:
            return '9'

    #handshake
    #check encryption
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def handshake(self):
        print "trying to handshake"
        input_data = cherrypy.request.json
        sender = input_data['sender']
        message = input_data['message']
        destination = input_data['destination']
        encryption = input_data['encryption']
        print "HANDSHAKE B4 DECRYPT"
        decrypted_message = self.decrypt(encryption, message)
        print decrypted_message
        if(decrypted_message == '15'):
            return '9'
        return decrypted_message

    #send file
    @cherrypy.expose  
    def sendFile(self, destination = None, file_attach = None):
        print "Send File"
        sender = cherrypy.session.get('username')
        password = cherrypy.session.get('password')
        ip = None
        port = None
        #encode files to base64
        f = base64.encodestring(file_attach.file.read())
        #get file name
        filename = file_attach.filename
        #get content_type/mime_type
        content_type = file_attach.content_type
        List = urllib2.urlopen("http://cs302.pythonanywhere.com/getList"+'?username='+ sender + '&password=' + password + '&enc=' +'0' +'&json=' + '1').read()
        data = json.loads(List)
        for i in data:
            if(str(destination) == data[i].get('username')):
                ip = data[i].get('ip')
                port = data[i].get('port')
                parameters = {'sender': sender, 'destination': destination, 'file': f, 'filename': str(filename), 'content_type': str(content_type), 'stamp': (time.time())}
                json_object = json.dumps(parameters)
                req = urllib2.Request("http://" + (ip) + ":" + (port) + "/receiveFile", json_object, {"Content-Type" :'application/json'})
                response = urllib2.urlopen(req)
                error = response.read()
                print error
                #give error message(if error = 0 then tell them it is succeed)
                error_message = self.errorMessage(error[0])
                Page = error_message
                back_button = self.backButton()
                Page += back_button
                return Page

                
        if(ip == None or port == None):
            error_message = self.errorMessage('1')
            Page = error_message
            back_button = self.backButton()
            Page += back_button
            return Page
            
           

    #save File
    def saveFile(self, data):
        f = base64.decodestring(data.get('file'))
        filename = data.get('filename')        
        f_result = open('files/' + filename, 'wb')
        f_result.write(f)
        
    #receive file
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def receiveFile(self):
        input_data = cherrypy.request.json

        print "receive File"
        #check ping
        if(self.ping(input_data["sender"]) == "0"):
            try:
                data= {
                    "sender" : (input_data["sender"]),
                    "destination" : (input_data["destination"]),
                    "file" : (input_data["file"]),
                    "filename" : (input_data["filename"]),
                    "content_type" : (input_data["content_type"]),
                    "stamp" : (input_data["stamp"])
                    }
                print "data in receive File"
                #saveFile
                self.saveFile(data)
                return "0"
            except:
                return "1"            
        else:
            return "3"                
            
        
    #receive Profile
    @cherrypy.expose
    def receiveProfile(self, profile_username = None):
        sender = cherrypy.session.get('username')
        password = cherrypy.session.get('password')
        ip = None
        port = None
        print "my details"
        print sender
        print "username"
        print profile_username
        print urllib2.urlopen("http://cs302.pythonanywhere.com/getList"+'?username='+ sender + '&password=' + password + '&enc=' +'0' +'&json=' + '1').read()
        List = urllib2.urlopen("http://cs302.pythonanywhere.com/getList"+'?username='+ sender + '&password=' + password + '&enc=' +'0' +'&json=' + '1').read()
        print "get List"
        data = json.loads(List)
        for i in data:
            print data[i].get('username')
            if(str(profile_username) == data[i].get('username')):
                print "got details"
                #if user using external ip, change it to local
                ip = data[i].get('ip')
                port = data[i].get('port')
                parameters = {'profile_username': profile_username, 'sender': sender}
                print parameters
                print "after parameters"
                json_object = json.dumps(parameters)
                print ip
                print port
                req = urllib2.Request("http://" + (ip) + ":" + (port) + "/getProfile", json_object, {"Content-Type" :'application/json'})
                response = urllib2.urlopen(req)
                print "what"
                profile_data = response.read()
                try:
                    print "why"
                    #save Profile
                    self.saveProfile(profile_username, profile_data)
                    Page = self.readProfile(profile_username)
                    back_button = self.backButton()
                    Page += back_button
                except:
                    #give them error message
                    error_message = self.errorMessage(profile_data)
                    Page = error_message
                    back_button = self.backButton()
                    Page += back_button
        #if there is no ip or port, then give them an error message       
        if(ip == None or port == None):
            error_message = self.errorMessage('1')
            Page = error_message
            back_button = self.backButton()
            Page += back_button
        return Page
            
    #get profile allow another user can take my profile
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def getProfile(self):
        input_data = cherrypy.request.json
        conn = sqlite3.connect('Profile.db')
        c = conn.cursor()
        c.execute("SELECT * From Profile")
        table = c.fetchall()
        data = None
        print data
        print "error 0"
        for i in table:
            #if find my data
            if(i[0]==input_data['profile_username']):
                data = i
        print data
        if(data == None):
            return "4"
        print "error 1"
        #make profile data as dictionary
        profile = {'lastUpdated': data[1], 'fullname': data[2], 'position': data[3],
            'description': data[4], 'location': data[5], 'picture': data[6]}
        #make profile as json object
        data = json.dumps(profile)
        return data
        
        

    #read profile
    @cherrypy.expose
    def readProfile(self, profile_username):
        Page = ''
        conn = sqlite3.connect('Profile.db')
        c = conn.cursor()
        try:
            c.execute("SELECT * From Profile")
            data = c.fetchall()
            for i in data:
                #read only one profile that we need
                if(str(i[0]) == str(profile_username)):
                    try:
                        Page += "<img src='" + i[6] + "' width='200' height='200' alt='Profile Image'><br/>"
                    except:
                        pass
                    Page += "profile_username : " + i[0] + '<br/>'
                    dateTime = self.dateTime(i[1])
                    Page += "Last Updated: " + str(dateTime) + '<br/>'
                    Page += "fullname : " + i[2] + '<br/>'
                    Page += "position : " + i[3] + '<br/>'
                    Page += "description : " + i[4] + '<br/>'
                    Page += "location : " + i[5] + '<br/>'
                    Page += '<br/>'
                conn.commit()
            conn.close()
        except:
            Page += "There is no Profile Yet <br/>"
            Page += "Please make Your Profile <br/>"
        return Page


    #read all profiles that I have
    @cherrypy.expose
    def readAllProfile(self):
        Page = ''
        conn = sqlite3.connect('Profile.db')
        c = conn.cursor()
        back_button = self.backButton()
        Page += back_button
        try:
            c.execute("SELECT * From Profile")
            data = c.fetchall()

            print data
            for i in data:
                Page += "<img src='" + i[6] + "' width='200' height='200' alt='Profile Image'><br/>"
                Page += "profile_username : " + i[0] + '<br/>'
                dateTime = self.dateTime(i[1])
                Page += "Last Updated: " + str(dateTime) + '<br/>'
                Page += "fullname : " + i[2] + '<br/>'
                Page += "position : " + i[3] + '<br/>'
                Page += "description : " + i[4] + '<br/>'
                Page += "location : " + i[5] + '<br/>'
                Page += '<br/>'
                conn.commit()
            conn.close()
        except:
            Page += "Here are All Profiles"
        return Page


    #save/change my profile
    @cherrypy.expose
    def saveMyProfile(self, fullname = None, position = None, description = None, url = None):
        print "print my profile"
        epoch_time = time.time()
        profile_username = cherrypy.session.get('username')
        print profile_username
        location = cherrypy.session.get('location')
        conn = sqlite3.connect('Profile.db')
        c = conn.cursor()
        c.execute("SELECT * From Profile")
        data = c.fetchall()
        for i in data:
            #find my profile data and save previous profile
            if(str(i[0]) == str(profile_username)):
                url_prev = str(i[6])
                fullname_prev = str(i[2])
                position_prev = str(i[3])
                description_prev = str(i[4])
            conn.commit()
        conn.close()
        #if user enter no value, then keep using a previous profile
        #if user eneter 'delete', then delete the value
        if(url == ''):
            url = url_prev
        elif(url == "delete"):
            url = ''
        else:
            pass
        if(fullname == ''):
            fullname = fullname_prev
        elif(fullname == "delete"):
            fullname = ''
        else:
            pass
        if(position == ''):
            position = position_prev
        elif(position == "delete"):
            position = ''
        else:
            pass
        if(description == ''):
            description = description_prev
        elif(description == "delete"):
            description = ''
        else:
            pass
            
        parameters = {'lastUpdated': epoch_time, 'fullname': str(fullname), 'position': str(position), 'description': str(description), 'location': location, 'picture': url}
        print parameters
        data = json.dumps(parameters)
        print data

        self.saveProfile(profile_username, data)
        raise cherrypy.HTTPRedirect('/')

    #save Profile
    def saveProfile(self, profile_username, data):
        data =json.loads(data)
        print "saveProfile"
        conn = sqlite3.connect('Profile.db')
        c = conn.cursor()
        print "open database"
        try: #create table using primary key, Profile username that only one equal value can exist and must not be Null
            c.execute('CREATE TABLE Profile (Profile_Username text PRIMARY KEY NOT NULL, LastUpdated real, Fullname text, Position text, Description text, Location text, URL text)')
        except:
            pass
        task = (
            (str(profile_username)),
            data.get('lastUpdated'),
            data.get('fullname'),
            data.get('position'),
            data.get('description'),
            data.get('location'),
            data.get('picture')
            )
        #insert or replace profile
        c.execute('INSERT OR REPLACE INTO Profile (Profile_Username, LastUpdated, Fullname, Position, Description, Location, URL) Values(?,?,?,?,?,?,?)', task)
        conn.commit()
        conn.close()        
          
def runMainApp():
    threading.Thread.damon = True
    # Create an instance of MainApp and tell Cherrypy to send all requests under / to it. (ie all of them)
    cherrypy.tree.mount(MainApp(), "/",
                        {'/files': {
                         'tools.staticdir.on': True,
                         'tools.staticdir.dir': abspath('./files')
                         }
                        }
                        )

    # Tell Cherrypy to listen for connections on the configured address and port.
    cherrypy.config.update({'server.socket_host': listen_ip,
                            'server.socket_port': listen_port,
                            'engine.autoreload.on': True,
                           })

    print "========================="
    print "University of Auckland"
    print "COMPSYS302 - Software Design Application"
    print "========================================"                       
    
    # Start the web server
    cherrypy.engine.start() 
    # And stop doing anything else. Let the web server take over.
    cherrypy.engine.block()
   
 
#Run the function to start everything
runMainApp()

